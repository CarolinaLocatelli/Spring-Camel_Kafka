package it.sisal.model;




public class Match {
	
	private int id;
	
	private String evento;
		
	private String disciplina;
	
	private String avvenimento;

	private String classe;
	
	private String esito;

	private String dataAvvenimento;

	private String quota;

	private String timestamp;
	
	public Match() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Match(int id, String evento, String disciplina, String avvenimento, String classe, String esito,
			String dataAvvenimento, String quota, String timestamp) {
		super();
		this.id = id;
		this.evento = evento;
		this.disciplina = disciplina;
		this.avvenimento = avvenimento;
		this.classe = classe;
		this.esito = esito;
		this.dataAvvenimento = dataAvvenimento;
		this.quota = quota;
		this.timestamp = timestamp;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(String disciplina) {
		this.disciplina = disciplina;
	}
	public String getAvvenimento() {
		return avvenimento;
	}
	public void setAvvenimento(String avvenimento) {
		this.avvenimento = avvenimento;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getDataAvvenimento() {
		return dataAvvenimento;
	}
	public void setDataAvvenimento(String dataAvvenimento) {
		this.dataAvvenimento = dataAvvenimento;
	}
	public String getQuota() {
		return quota;
	}
	public void setQuota(String quota) {
		this.quota = quota;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public String toString() {
		return "Match [id=" + id + ", evento=" + evento + ", disciplina=" + disciplina + ", avvenimento=" + avvenimento
				+ ", classe=" + classe + ", esito=" + esito + ", dataAvvenimento=" + dataAvvenimento + ", quota="
				+ quota + ", timestamp=" + timestamp + "]";
	} 
	
	

}
