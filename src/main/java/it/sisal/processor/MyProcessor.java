package it.sisal.processor;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import it.sisal.model.Match;

public class MyProcessor implements Processor {
	private List<Match> matches = new ArrayList();
	private boolean flagAggiungi = true;

	public void process(Exchange exchange) throws Exception {


		System.out.println("::I am processing what Kafka sends::");


		//Test iniziale: controllo se kafka legge correttamente dal csv (ogni elemento diviso per riga, ogni campo diviso dalla virgola)
		if (exchange.getIn() != null) {
			Message message = exchange.getIn();

			try {
				//prendo il corpo del messaggio
				List<String> data =  (List<String>) message.getBody();

				//costruisco un oggetto Match popolandolo dai dati del corpo del messaggio
				Match m = new Match(Integer.parseInt(data.get(0)), data.get(1), data.get(2), data.get(3), data.get(4), data.get(5), data.get(6), data.get(7), data.get(8));

				/**modifico la lista dei messaggi ricevuti considerando solo i valori più "fresh" per non sprecare banda*/

				for(int i=0; i<matches.size(); i++){

					//se il messaggio è già stato inserito in precedenza, non lo considero
					if(matches.get(i).getId()==m.getId()){
						flagAggiungi = false;
					}

					//se il nuovo messaggio contiene dati su un esito già inserito, verifico se bisogna aggiornare la quota
					if(matches.get(i).getEvento().equals(m.getEvento()) && matches.get(i).getClasse().equals(m.getClasse()) && matches.get(i).getEsito().equals(m.getEsito())){

						SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
						Date parsedDate = dateFormat.parse(matches.get(i).getTimestamp());
						Timestamp timestampInLista = new java.sql.Timestamp(parsedDate.getTime());
						parsedDate = dateFormat.parse(m.getTimestamp());
						Timestamp timestampNuovo = new java.sql.Timestamp(parsedDate.getTime());

						//se il timestamp è più recente, rimuovo il match contenente la vecchia quota non aggiornata e aggiungo il nuovo
						if(timestampNuovo.after(timestampInLista) && timestampNuovo!=timestampInLista && !timestampNuovo.before(timestampInLista)){
							matches.remove(matches.get(i));
						} else {
							flagAggiungi=false;
						}
					}           	 
				} 

				if(flagAggiungi){
					matches.add(m);
				}

			} catch (Exception e){
				e.printStackTrace();
			}             
		}

		//una volta ricavata una lista di partite aggiornate, invio ciascun elemento sottoforma di json
		for(Match match : matches){
			//converto Match in json (String)
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String jsonString = ow.writeValueAsString(match);
			System.out.println(jsonString);

			//risposta del processor: per ogni match convertito in json invio un messaggio
			exchange.getOut().setBody(jsonString);

		}


	}

}
