package it.sisal.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

import it.sisal.processor.MyProcessor;

public class SimpleRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
    	
    	//Leggo dal csv file (!!!BISOGNA SALVARLO NELLA DIRECTORY C:/inputFolder!!!)
    	from("file:C:/inputFolder?noop=true") 
        .unmarshal().csv()
        .split(body())
        //passo a kafka ciascuna riga del csv e splitto ciascun campo
        .to("kafka:localhost:9092?topic=test&consumersCount=1")
        
        //ricevo i messaggi di kafka e mantengo solo quelli più fresh (quote aggiornate)
        .process(new MyProcessor())
       
        //chiamo servizio REST che gira sul server e aggiorna tutte le quote
        .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
        .to("http://localhost:8080/SpringRESTApp/matches");
        
    	
    	
  
        }

}
